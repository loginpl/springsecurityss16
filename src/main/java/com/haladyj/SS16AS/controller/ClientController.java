package com.haladyj.SS16AS.controller;

import com.haladyj.SS16AS.model.Client;
import com.haladyj.SS16AS.model.User;
import com.haladyj.SS16AS.repository.ClientRepository;
import com.haladyj.SS16AS.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    private ClientRepository clientRepository;

    @PostMapping("/create")
    public ResponseEntity<Client> createClient(@RequestBody Client client){
        clientRepository.save(client);
        return ResponseEntity.ok(client);
    }
}
