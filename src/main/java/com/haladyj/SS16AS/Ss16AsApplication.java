package com.haladyj.SS16AS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ss16AsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ss16AsApplication.class, args);
	}

}
