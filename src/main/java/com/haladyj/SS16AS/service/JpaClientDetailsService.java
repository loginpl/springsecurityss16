package com.haladyj.SS16AS.service;

import com.haladyj.SS16AS.model.Client;
import com.haladyj.SS16AS.repository.ClientRepository;
import com.haladyj.SS16AS.security.model.SecurityClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Service;

@Service
public class JpaClientDetailsService implements ClientDetailsService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {

        Client client = clientRepository.findClientByClientId(clientId)
                .orElseThrow(()->new ClientRegistrationException("Client does not exist"));

        return new SecurityClient(client);
    }
}
