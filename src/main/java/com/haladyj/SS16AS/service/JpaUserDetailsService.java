package com.haladyj.SS16AS.service;

import com.haladyj.SS16AS.repository.UserRepository;
import com.haladyj.SS16AS.security.model.SecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class JpaUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        var user = userRepository.findUserByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Not found in DB"));
        return new SecurityUser(user);
    }
}
