package com.haladyj.SS16AS.security.model;

import com.haladyj.SS16AS.model.Client;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

import java.util.*;
import java.util.stream.Collectors;

public class SecurityClient implements ClientDetails {

    private final Client client;

    public SecurityClient(Client client) {
        this.client = client;
    }

    @Override
    public String getClientId() {
        return client.getClientId();
    }

    @Override
    public Set<String> getResourceIds() {
        return null;
    }

    @Override
    public boolean isSecretRequired() {
        return true;
    }

    @Override
    public String getClientSecret() {
        return client.getSecret();
    }

    @Override
    public boolean isScoped() {
        return true;
    }

    @Override
    public Set<String> getScope() {
        List<String> clientScopes = client.getScopes();
        Set<String> scopes = new HashSet(clientScopes);
        return scopes;
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        List<String> clientGrantTypes = client.getGrant_types();
        Set<String> grantTypes = new HashSet(clientGrantTypes);
        return grantTypes;
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        return null;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        List<String> userAuthorities = client.getAuthorities();

        return userAuthorities.stream().map(auth ->new GrantedAuthority() {
            @Override
            public String getAuthority() {
                return auth;
            }
        }).collect(Collectors.toList());
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return null;
    }

    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return null;
    }

    @Override
    public boolean isAutoApprove(String scope) {
        return false;
    }

    @Override
    public Map<String, Object> getAdditionalInformation() {
        return null;
    }
}
