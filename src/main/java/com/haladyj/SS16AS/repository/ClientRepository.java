package com.haladyj.SS16AS.repository;

import com.haladyj.SS16AS.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client,Integer> {

    Optional<Client> findClientByClientId(String clientId);
}
