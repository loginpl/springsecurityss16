package com.haladyj.SS16AS.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String clientId;
    private String secret;

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(name="client_scopes", joinColumns = @JoinColumn(name="id_client"))
    private List<String> scopes;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name="client_grant_types", joinColumns = @JoinColumn(name="id_client"))
    private List<String> grant_types;

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(name="client_authorities", joinColumns = @JoinColumn(name="id_client"))
    private List<String> authorities;

    public Client() {
    }

    public Client(Integer id, String clientId, String secret, List<String> scopes, List<String> grant_types, List<String> authorities) {
        this.id = id;
        this.clientId = clientId;
        this.secret = secret;
        this.scopes = scopes;
        this.grant_types = grant_types;
        this.authorities = authorities;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public List<String> getScopes() {
        return scopes;
    }

    public void setScopes(List<String> scopes) {
        this.scopes = scopes;
    }

    public List<String> getGrant_types() {
        return grant_types;
    }

    public void setGrant_types(List<String> grant_types) {
        this.grant_types = grant_types;
    }

    public List<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<String> authorities) {
        this.authorities = authorities;
    }
}
